/*
 * Copyright (c) 2006-2019 Henri Tremblay.
 */
package interview.questions.question1;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Please implement the {@link #mostPopularTicker(List)} method. With a sequential and a parallel algorithm.
 * DO NOT modify the existing code.
 */
public final class TransactionStatistics {

   /**
    * IMPORTANT
    *
    * I could not find a better way of implementing
    * static method
    * with 2 algorithms
    * in a final class
    * without modification of existing code.
    *
    * If it was a real development task at work I would discuss the requirements.
    */

   public enum Algorithm {
      SEQUENTIAL,
      PARALLEL;
   }

   private static Algorithm algorithm;

   public static void setAlgorithm(Algorithm algorithm){
      TransactionStatistics.algorithm = algorithm;
   }

   /**
    * Return the most popular ticker in terms of transaction total absolute value (i.e. abs(price * quantity)). For example,
    * let's say we have these transactions:
    * <ul>
    *     <li>transaction("a", -10, 2.0)</li>
    *     <li>transaction("a", 20, 1.0)</li>
    *     <li>transaction("b", 5, 1.0)</li>
    *     <li>transaction("b", 10, 1.0)</li>
    *     <li>transaction("b", 10, 1.0)</li>
    * </ul>
    * The most popular one ticker is "a" with a traded value of 40 compared to b that only has 25.
    *
    * @param list List containing all transactions we want to look at
    * @return the most popular ticker
    */
   public static String mostPopularTicker(List<Transaction> transactions) {
      if (Objects.equals(algorithm, Algorithm.SEQUENTIAL)){
         return mostPopularTickerSeq(transactions);
      } else if (Objects.equals(algorithm, Algorithm.PARALLEL)){
         return mostPopularTickerParallel(transactions);
      }
      return null;
   }

   private TransactionStatistics()  {}

   private static String mostPopularTickerSeq(List<Transaction> transactions) {
      Map<String, Double> aggregated =  transactions.stream()
         .collect(Collectors.groupingBy(t -> t.getTicker(), Collectors.summingDouble(t -> Math.abs(t.getPrice() * t.getQuantity()))));
      return aggregated.entrySet().stream().max((entry1, entry2) -> entry1.getValue() > entry2.getValue() ? 1 : -1).get().getKey();
   }

   private static String mostPopularTickerParallel(List<Transaction> transactions) {
      Map<String, Double> aggregated =  transactions.parallelStream()
         .collect(Collectors.groupingBy(t -> t.getTicker(), Collectors.summingDouble(t -> Math.abs(t.getPrice() * t.getQuantity()))));
      return aggregated.entrySet().stream().max((entry1, entry2) -> entry1.getValue() > entry2.getValue() ? 1 : -1).get().getKey();
   }
}



