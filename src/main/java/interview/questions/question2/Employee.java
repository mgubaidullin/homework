/*
 * Copyright (c) 2006-2019 Henri Tremblay.
 */
package interview.questions.question2;

import java.util.Objects;

public class Employee extends Person {
    private final String role;

    public Employee(String name, int age, String role) {
        super(name, age);
        this.role = role;
    }

   @Override
   public String toString() {
      return getClass().getSimpleName() + " " + getName() + " has " + getAge() + " years old and role " + role;
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      if (!super.equals(o)) return false;
      Employee employee = (Employee) o;
      return this.getAge() == employee.getAge()
         && this.getName().equals(employee.getName()) && this.role.equals(employee.role);
   }

   @Override
   public int hashCode() {
      return Objects.hash(super.hashCode(), role);
   }
}
