package interview.questions.question3;

import java.util.concurrent.atomic.AtomicReference;
/**
 * AtomicReference implementation of Statistic.
 */
public class StatisticAtomic implements Statistic {

   private AtomicReference<Data> atomicData = new AtomicReference<>(new Data(0, 0, 0, 0, 0F));

   public long count(){
      return atomicData.get().getCount();
   }

   @Override
   public void event(int value) {
      Data data1, data2;
      do {
         data1 = atomicData.get();
         long s = data1.getSum() + value;
         long c = data1.getCount() + 1;
         float m = s / c;
         int max = (value > data1.getMaximum()) ? value : data1.getMaximum();
         int min = (value < data1.getMinimum()) ? value : data1.getMinimum();
         data2 = new Data(c, s, min, max, m);
      } while (!atomicData.compareAndSet(data1, data2));
   }

   @Override
   public float mean() {
      if (atomicData.get().getCount() == 0) throw new RuntimeException("No data");
      return atomicData.get().getMean();
   }

   @Override
   public int minimum() {
      if (atomicData.get().getCount() == 0) throw new RuntimeException("No data");
      return atomicData.get().getMinimum();
   }

   @Override
   public int maximum() {
      if (atomicData.get().getCount() == 0) throw new RuntimeException("No data");
      return atomicData.get().getMaximum();
   }

   private class Data {
      private final long count;
      private final long sum;
      private final int minimum;
      private final int maximum;
      private final float mean;

      public Data(long count, long sum, int minimum, int maximum, float mean) {
         this.count = count;
         this.sum = sum;
         this.minimum = minimum;
         this.maximum = maximum;
         this.mean = mean;
      }

      @Override
      public String toString() {
         return "Data{" +
            "count=" + count +
            ", sum=" + sum +
            ", minimum=" + minimum +
            ", maximum=" + maximum +
            ", mean=" + mean +
            '}';
      }

      public long getCount() {
         return count;
      }

      public long getSum() {
         return sum;
      }

      public int getMinimum() {
         return minimum;
      }

      public int getMaximum() {
         return maximum;
      }

      public float getMean() {
         return mean;
      }
   }
}
