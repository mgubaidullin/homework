package interview.questions.question3;
/**
 * Synchronized implementation of Statistic.
 */
public class StatisticSynchronized implements Statistic {

   private long count = 0;
   private long sum = 0;
   private int minimum = 0;
   private int maximum = 0;
   private float mean = 0F;
   private float variance = 0F;
   private float squareSum = 0F;

   @Override
   synchronized public void event(int value) {
      sum = sum + value;
      count++;
      mean = sum / count;
      maximum = (value > maximum) ? value : maximum;
      minimum = (value < minimum) ? value : minimum;

      squareSum = squareSum + (value * value);
      variance = (squareSum - count * mean * sum + count * (mean * mean)) / count;
   }

   @Override
   public float mean() {
      if (count == 0) throw new RuntimeException("No data");
      return mean;
   }

   @Override
   public int minimum() {
      if (count == 0) throw new RuntimeException("No data");
      return minimum;
   }

   @Override
   public int maximum() {
      if (count == 0) throw new RuntimeException("No data");
      return maximum;
   }

   @Override
   public float variance() {
      return variance;
   }
}

