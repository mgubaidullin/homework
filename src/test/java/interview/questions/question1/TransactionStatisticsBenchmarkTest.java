package interview.questions.question1;

import org.junit.Test;
import org.openjdk.jmh.annotations.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

@State(Scope.Benchmark)
@Threads(1)
@Fork(value = 1, warmups = 1)
@Warmup(iterations = 3)
@Measurement(iterations = 5)
@BenchmarkMode(Mode.AverageTime)
public class TransactionStatisticsBenchmarkTest {

   @Test
   public void benchmark() throws Exception {
      String[] argv = {};
      org.openjdk.jmh.Main.main(argv);
   }

   @Benchmark
   public void performanceMostPopularSeq() {
      TransactionStatistics.setAlgorithm(TransactionStatistics.Algorithm.SEQUENTIAL);
      TransactionStatistics.mostPopularTicker(generateTransactions());
   }

   @Benchmark
   public void performanceMostPopularParallel() {
      TransactionStatistics.setAlgorithm(TransactionStatistics.Algorithm.PARALLEL);
      TransactionStatistics.mostPopularTicker(generateTransactions());
   }

   private List<Transaction> generateTransactions() {
      Random random = new Random(7654321);
      List<Transaction> transactions = new ArrayList<>(1000000);
      IntStream.range(0, 1000000).forEach(value -> {
         String ticket = "T" + random.nextInt(40);
         int quantity = random.nextInt(20) * (random.nextBoolean() ? 1: -1);
         double price = random.nextDouble() * 10;
         transactions.add(Transaction.transaction(ticket, quantity, price));
      });
      return transactions;
   }
}
