package interview.questions.question1;

import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class TransactionStatisticsTest {

   @Test
   public void findMostPopularSeq() {
      TransactionStatistics.setAlgorithm(TransactionStatistics.Algorithm.SEQUENTIAL);
      String ticket = TransactionStatistics.mostPopularTicker(generateTransactions());
      Assertions.assertThat(ticket).isEqualTo("a");
   }

   @Test
   public void findMostPopularParallel() {
      TransactionStatistics.setAlgorithm(TransactionStatistics.Algorithm.PARALLEL);
      String ticket = TransactionStatistics.mostPopularTicker(generateTransactions());
      Assertions.assertThat(ticket).isEqualTo("a");
   }

   private List<Transaction> generateTransactions() {
      List<Transaction> transactions = new ArrayList();
      transactions.add(Transaction.transaction("a", -10, 2.0));
      transactions.add(Transaction.transaction("a", 20, 1.0));
      transactions.add(Transaction.transaction("b", 5, 1.0));
      transactions.add(Transaction.transaction("b", 10, 1.0));
      transactions.add(Transaction.transaction("b", 10, 1.0));
      return transactions;
   }
}
