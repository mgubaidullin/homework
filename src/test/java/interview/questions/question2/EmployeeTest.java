package interview.questions.question2;

import org.assertj.core.api.Assertions;
import org.junit.Test;

public class EmployeeTest {

   @Test
   public void checkEquals() {
      Employee employee1 = new Employee("john", 10, "dev");
      Employee employee2 = new Employee("john", 10, "dev");
      Assertions.assertThat(employee1).isEqualTo(employee2);
   }

   @Test
   public void checkNonEquals() {
      Employee employee1 = new Employee("john", 10, "dev");
      Employee employee2 = new Employee("bob", 10, "dev");
      Assertions.assertThat(employee1).isNotEqualTo(employee2);
   }

   @Test
   public void checkInstanceNotEquals() {
      Employee employee = new Employee("john", 10, "dev");
      Person person = new Person("john", 10);
      Assertions.assertThat(employee).isNotEqualTo(person);
   }
}
