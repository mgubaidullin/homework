package interview.questions.question2;

import org.assertj.core.api.Assertions;
import org.junit.Test;

public class PersonTest {

   @Test
   public void checkEquals() {
      Person person1 = new Person("john", 10);
      Person person2 = new Person("john", 10);
      Assertions.assertThat(person1).isEqualTo(person2);
   }

   @Test
   public void checkNonEquals() {
      Person person1 = new Person("john", 10);
      Person person2 = new Person("bob", 10);
      Assertions.assertThat(person1).isNotEqualTo(person2);
   }
}
