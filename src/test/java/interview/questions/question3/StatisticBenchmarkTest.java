package interview.questions.question3;

import io.vavr.control.Try;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.openjdk.jmh.annotations.*;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@State(Scope.Benchmark)
@Threads(1)
@Fork(value = 1, warmups = 1)
@Warmup(iterations = 3)
@Measurement(iterations = 5)
@BenchmarkMode(Mode.AverageTime)
public class StatisticBenchmarkTest {

   private static final int RANGE = 1000000;

   @Test
   public void benchmark() throws Exception {
      String[] argv = {};
      org.openjdk.jmh.Main.main(argv);
   }

   @Benchmark
   public void performanceStatisticSynchronized() {
      test(new StatisticSynchronized());
   }

   @Benchmark
   public void performanceStatisticAtomic() {
      test(new StatisticAtomic());
   }

   private void test(Statistic statistic) {
      List<CompletableFuture<Void>> futures = IntStream.range(1, 10)
         .mapToObj(value -> start(statistic, value * RANGE, value * RANGE + RANGE))
         .collect(Collectors.toList());
      futures.forEach(f -> Try.of(() -> f.get()));
      Assertions.assertThat(statistic.minimum()).isEqualTo(0);
      Assertions.assertThat(statistic.maximum()).isEqualTo(10 * RANGE - 1);
      Assertions.assertThat(statistic.mean()).isEqualTo(5499999);
   }

   private CompletableFuture<Void> start(Statistic statistic, int startInclusive, int endExclusive) {
      return CompletableFuture.runAsync(() -> IntStream.range(startInclusive, endExclusive).forEach(value -> statistic.event(value)));
   }
}
