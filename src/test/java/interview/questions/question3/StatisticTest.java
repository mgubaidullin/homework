package interview.questions.question3;

import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.util.stream.IntStream;

public class StatisticTest {

   private static final int RANGE = 1000000;

   @Test
   public void testAtomic() {
      test(new StatisticAtomic());
   }

   @Test
   public void testSynchronized() {
      test(new StatisticSynchronized());
   }

   public void test(Statistic statistic) {
      IntStream.range(RANGE, RANGE * 10).forEach(value -> statistic.event(value));
      int min = statistic.minimum();
      int max = statistic.maximum();
      float mean = statistic.mean();
      Assertions.assertThat(min).isEqualTo(0);
      Assertions.assertThat(max).isEqualTo(10 * RANGE - 1);
      Assertions.assertThat(mean).isEqualTo(5499999.0F);
   }
}
